package ru.t1.sochilenkov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.endpoint.*;
import ru.t1.sochilenkov.tm.api.service.*;
import ru.t1.sochilenkov.tm.endpoint.*;
import ru.t1.sochilenkov.tm.service.*;
import ru.t1.sochilenkov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    @Getter
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    @Getter
    private final IUserService userService = new UserService(propertyService, connectionService);

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @NotNull
    @Getter
    private final ISessionService sessionService = new SessionService(connectionService);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    final IUserEndpoint userEndpoint = new UserEndpoint(this);

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final String port = getPropertyService().getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER SERVER IS SHUTTING DOWN **");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void start() {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);

        initPID();
        loggerService.info("** WELCOME TO TASK MANAGER SERVER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

}
