package ru.t1.sochilenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.model.Session;
import ru.t1.sochilenkov.tm.model.User;

public interface IAuthService {

    @Nullable
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    void invalidate(Session session) throws Exception;

    @NotNull
    Session validateToken(@Nullable String token);

}
