package ru.t1.sochilenkov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.api.repository.IProjectRepository;
import ru.t1.sochilenkov.tm.api.repository.ITaskRepository;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.IProjectTaskService;
import ru.t1.sochilenkov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sochilenkov.tm.exception.entity.TaskNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.*;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.model.Task;

import java.sql.Connection;
import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable final Task task;
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            taskRepository.update(task);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final List<Task> tasks;
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            tasks = taskRepository.findAllByProjectId(userId, projectId);
            for (final Task task : tasks) taskRepository.removeById(userId, task.getId());
            projectRepository.removeById(userId, projectId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeProjectByIndex(@Nullable final String userId, @Nullable final Integer projectIndex) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectIndex == null || projectIndex < 0) throw new IndexIncorrectException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final List<Task> tasks;
        @Nullable Project project;
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            project = projectRepository.findOneByIndex(userId, projectIndex);
            if (project == null) throw new ProjectNotFoundException();
            tasks = taskRepository.findAllByProjectId(userId, project.getId());
            for (final Task task : tasks) taskRepository.removeById(userId, task.getId());
            projectRepository.removeById(userId, project.getId());
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String userId,
                                      @Nullable final String projectId,
                                      @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable final Task task;
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            taskRepository.update(task);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
