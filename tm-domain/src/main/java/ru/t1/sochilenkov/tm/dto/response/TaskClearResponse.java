package ru.t1.sochilenkov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.model.Task;

@NoArgsConstructor
public final class TaskClearResponse extends AbstractTaskResponse {

    public TaskClearResponse(@Nullable final Task task) {
        super(task);
    }

}
